<?php

namespace Drupal\bbma_menu\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Render\Markup;

/**
 * Provides a 'BbmaMenuBlock' block.
 *
 * @Block(
 *  id = "bbma_menu_block",
 *  admin_label = @Translation("Biblissima side menu"),
 * )
 */
class BbmaMenuBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    if (stream_resolve_include_path('menu-bbma.php')) {
      ob_start();
      require_once('menu-bbma.php');
      $bbma_menu = ob_get_clean();
      $build['bbma_menu_block']['#markup'] = Markup::create($bbma_menu);
    }

    return $build;
  }

}
